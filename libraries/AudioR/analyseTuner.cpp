

#include "analyseTuner.h"
#include "utility/dspinst.h"


#define TUNACOUNTTHRESHOLD (AUDIO_SAMPLE_RATE_EXACT/TUNAMINIMUMFREQUENCY)
#define TUNAINTERVALSTEP (1.0f/AUDIO_SAMPLE_RATE_EXACT)
#define OCTAVA	31.785

#define SPS_FALLING 0
#define SPS_RISING 1

#define FTF_FALLING (1<<SPS_FALLING)
#define FTF_RISING	(1<<SPS_RISING)

TUNAPRECISIONHI fmapf(TUNAPRECISIONHI x, TUNAPRECISIONHI in_min, TUNAPRECISIONHI in_max, TUNAPRECISIONHI out_min, TUNAPRECISIONHI out_max)
{
  if(out_max>out_min)
  {
    return (x-in_min)*(out_max-out_min)/(in_max-in_min)+out_min;
  } else {
    return -((x-in_max)*(out_max-out_min))/(in_min-in_max)+out_max;
  }
}

uint8_t analyseTuner::nPtr(uint8_t n)
{
	uint8_t r=sPtr[n];
	sPtr[n]=(sPtr[n]+1)&7;
	return r;
}

void analyseTuner::update(void)
{
	audio_block_t *block;
	const int16_t *p, *end;
	static TUNAPRECISIONHI fcm[4]={0,0,0,0};
	static float aprev1=0,aprev2=0,bprev1=0,bprev2=0;
	TUNAPRECISIONHI tt=0;

	block = receiveReadOnly();
	if (!block) {
		return;
	}
	p = block->data;
	end = p + AUDIO_BLOCK_SAMPLES;
	do {
		int16_t d=*p++;
		// apply low pass filter to the data before 'considering' it. - perhaps consider changing this to fixed integer form for sake of speed.
		float sum=(float)d*0.00354707613586822f;
		sum+=bprev1*0.00709415227173644f;
		sum+=bprev2*0.00354707613586822f; // it is a low pass at 1000Hz, Q=0.7071
		sum+=aprev1*1.82305695391694f;
		sum+=aprev2*-0.83724525846041f;
		aprev2=aprev1;
		aprev1=sum;
		bprev2=bprev1;
		bprev1=(float)d;
		d=saturate16((int32_t)aprev1);
		dp[0].count++;
		if(dp[0].dir>0) // 
		{
			if(dp[0].count>TUNACOUNTTHRESHOLD/2)			
			{
				fcm[3]=TUNAINTERVALTHRESHOLD/2;
				inSamples[SPS_RISING][nPtr(SPS_RISING)]=(TUNAPRECISIONLO)fcm[0]+fcm[1]+fcm[2]+fcm[3];
				fcm[0]=TUNAINTERVALTHRESHOLD/2;
				freshTunaFlag|=FTF_RISING;
				dp[1].data=dp[0].data;
				dp[0].data=d;
				dp[0].count=0;
				dp[0].dir=-1;
			} else {
				if(d>dp[0].data) dp[0].data=d;
				if((float)d<-0.85f*(float)dp[0].data)
				{
					if(dp[0].data>300)
					{
						tt=(TUNAPRECISIONHI)dp[0].count*TUNAINTERVALSTEP; // the entire rising interval
						
						// Test this following line as thoroughly as possible, the timing it returns must reflect the (*imaginary) location of the '0' crossing as accurately as possibly.
						fcm[3]=fmapf(0,(TUNAPRECISIONHI)dp[1].data,(TUNAPRECISIONHI)dp[0].data,0,tt);
						
						inSamples[SPS_RISING][nPtr(SPS_RISING)]=(TUNAPRECISIONLO)fcm[0]+fcm[1]+fcm[2]+fcm[3];
						fcm[0]=tt-fcm[3];
						freshTunaFlag|=FTF_RISING;
						dp[1].data=dp[0].data;
						dp[0].data=d;
						dp[0].count=0;
						dp[0].dir=-1;
					} // we ignore stuff under 300/32767
				}
			}
		} else { // dp[0].dir must be 0 or <0
			if(dp[0].count>TUNACOUNTTHRESHOLD/2)			
			{
				fcm[1]=TUNAINTERVALTHRESHOLD/2;
				inSamples[SPS_FALLING][nPtr(SPS_FALLING)]=(TUNAPRECISIONLO)fcm[0]+fcm[1]+fcm[2]+fcm[3];
				fcm[2]=TUNAINTERVALTHRESHOLD/2;
				freshTunaFlag|=FTF_FALLING;
				dp[1].data=dp[0].data;
				dp[0].data=d;
				dp[0].count=0;
				dp[0].dir=1;
			} else {
			
				if(d<dp[0].data) dp[0].data=d;
				if((float)d>-0.85f*(float)dp[0].data)
				{
					if(dp[0].data<0&&abs(dp[0].data)>300)
					{
						tt=(TUNAPRECISIONHI)dp[0].count*TUNAINTERVALSTEP;
						
						// test test test test, not sure, are either of these right at all?
	// which is right?	fcm[1]=fmapf(0,(TUNAPRECISIONHI)dp[0].data,(TUNAPRECISIONHI)dp[1].data,tt,0);
						fcm[1]=fmapf(0,(TUNAPRECISIONHI)dp[0].data,(TUNAPRECISIONHI)dp[1].data,0,tt);
						
						inSamples[SPS_FALLING][nPtr(SPS_FALLING)]=(TUNAPRECISIONLO)fcm[0]+fcm[1]+fcm[2]+fcm[3];
						fcm[2]=tt-fcm[1];
						freshTunaFlag|=FTF_FALLING;
						dp[1].data=dp[0].data;
						dp[0].data=d;
						dp[0].count=0;
						dp[0].dir=1;
					}
				}
			}
		}
	} while (p < end);
	release(block);
}


TUNAFLAGVARTYPE analyseTuner::read(void)
{
	static TUNAPRECISIONLO cDiff=0,nDiff=0,cTol=0;
	static uint8_t state=0,c1=0,c2=0,c3=0,cPop=8,nPop=0;
	static TUNAPRECISIONLO tFreq=0,nFreq=0;
	
	if(flags&TUNAFRESHTUNA) flags&=~TUNAFRESHTUNA; // they cannot turn it off, we had better :)
	
	switch(state) { // we pat our dog, but only just pat, as often as we are called
// 	case 0: see default: ...
	case 1:
		if(freshTunaFlag&FTF_RISING)
		{
			freshTunaFlag&=~FTF_RISING;
			__disable_irq();
			pdSamples[4]=(inSamples[SPS_RISING][nPtr(SPS_RISING)]+inSamples[SPS_RISING][nPtr(SPS_RISING)])/2;
			pdSamples[5]=(inSamples[SPS_RISING][nPtr(SPS_RISING)]+inSamples[SPS_RISING][nPtr(SPS_RISING)])/2;
			pdSamples[6]=(inSamples[SPS_RISING][nPtr(SPS_RISING)]+inSamples[SPS_RISING][nPtr(SPS_RISING)])/2;
			pdSamples[7]=(inSamples[SPS_RISING][nPtr(SPS_RISING)]+inSamples[SPS_RISING][nPtr(SPS_RISING)])/2;
			Serial.printf("pdSamples[4-7]={%f,%f,%f,%f}-Rising\n",pdSamples[4],pdSamples[5],pdSamples[6],pdSamples[7]);
			__enable_irq();
		}
		state=2;
	break;
	case 2:
		if(freshTunaFlag&FTF_FALLING)
		{
			freshTunaFlag&=~FTF_FALLING;
			__disable_irq();
			pdSamples[0]=(inSamples[SPS_FALLING][nPtr(SPS_FALLING)]+inSamples[SPS_FALLING][nPtr(SPS_FALLING)])/2;
			pdSamples[1]=(inSamples[SPS_FALLING][nPtr(SPS_FALLING)]+inSamples[SPS_FALLING][nPtr(SPS_FALLING)])/2;
			pdSamples[2]=(inSamples[SPS_FALLING][nPtr(SPS_FALLING)]+inSamples[SPS_FALLING][nPtr(SPS_FALLING)])/2;
			pdSamples[3]=(inSamples[SPS_FALLING][nPtr(SPS_FALLING)]+inSamples[SPS_FALLING][nPtr(SPS_FALLING)])/2;
			Serial.printf("pdSamples[0-3]={%f,%f,%f,%f}-Rising\n",pdSamples[0],pdSamples[1],pdSamples[2],pdSamples[3]);
			__enable_irq();
		}
		// prepare variables for popularity contest as well - pdSamples are all treated equally from this point on.
		c1=255; // The one the others are being compared to
		c2=8; // the one being compared to c1
		state=3;
	break;
	case 3: // popularity contest
		if(c2<8)
		{
			if(fabs(pdSamples[c1]-pdSamples[c2])<cTol) c3++;
			c2++;
		} else {
			if(c3>nPop)
			{
				cPop=c1;
				nPop=c3;
			}
			c1++;
			if(c1<8)
			{
				if(c1==0)
				{
					cPop=8;
					nPop=0;
				}
				cTol=tolSamples*pdSamples[c1];
				c2=0;
			} else {
				state=4; // get the hell out of here.
			}
			c3=1; // there is always one matching sample for any given sample - itself, this is so as not to fuck up averaging later
		}
		if(c2==c1) c2++; // do not compare to self please, we already know it matches.
	break;

	case 4:
		if(nPop<minMatchPairs)
		{
			if(flags&TUNAFLAGDTCTG) flags=(flags&~TUNAFLAGDTCTG)|TUNAFRESHTUNA; // not detecting and we can signal that.
			state=0;
		} else {
			// flags|=TUNAFLAGDTCTG; // detecting... - turn this on later m8
			c1=255;
			fAccum=0;
			state=5;
		}
	break;
	
	case 5:
		c1++;
		// if(c1==cPop) c1++;
		if(c1<8)
		{
			if(fabs(pdSamples[cPop]-pdSamples[c1])<cTol) fAccum+=pdSamples[c1];
		} else {
			fAccum/=(float)nPop; // average me baby.
			if(fAccum<TUNAINTERVALTHRESHOLD)
			{
				nFreq=tFreq=1.0f/fAccum;
				while(nFreq>OCTAVA) nFreq/=2;
				// flags|=TUNAFLAGDTCTG; - lol, nope, not yet, you eager fucker.
				c1=255;
				cPop=12;
				cDiff=30;
				state=7;
			} else {
				flags=(flags&~(TUNAFLAGDTCTG|31))|TUNAFRESHTUNA|TUNAFLAGLOOSE|TUNAFLAGTUNED|TUNAFLAGTIGHT|12; // all indicators off, note set to '-'
				detDiff=0;
				detFreq=0;
				// ? flags&=~TUNAFLAGDTCTG;
				state=0;
			}
		}
	break;
	
	// case 6: - dunno why but I do not feel like having a case 6 in this one :p
	
	case 7:
		c1++;
		if(c1<12)
		{
			nDiff=-(notes[c1]-nFreq);
			if(fabs(nDiff)<fabs(cDiff))
			{
				cDiff=nDiff; // maintain sign.
				cPop=c1;
			}
		} else {
			detDiff=-(nFreq-notes[cPop])*ratios[cPop];
			cTol=FreqTol(notes[cPop]);
			if(tFreq!=detFreq||cDiff*ratios[cPop]!=detDiff)
			{
				detFreq=tFreq;
				detDiff=cDiff*ratios[cPop]; // will the display get this around the right way?
				state=8;
			} else {
				flags=(flags^TUNAFLAGDTCTG)|TUNAFRESHTUNA; // as we've just measured same more than once we may as well flash away :)
			}
		}
	break;
	case 8:
		flags=(flags&~31)|TUNAFLAGDTCTG|TUNAFRESHTUNA|TUNAFLAGLOOSE|TUNAFLAGTUNED|TUNAFLAGTIGHT|cPop;
		if(fabs(cDiff)<=cTol)
		{
			flags&=~TUNAFLAGTUNED;
		} else {
			if(cDiff<0) flags&=~TUNAFLAGLOOSE; else flags&=~TUNAFLAGTIGHT;
		}
		state=9; // just mucking around, this derives default as well as 0 does ;)
	// break; - let execution roll on to default below - perfectly OK to immediately go back around if we have fresh fish.
	default:
		if(freshTunaFlag) state=1; // we park here until we have fresh fish to dissect, it doesn't particularly matter where 'here' is (tho, likely to be state==0) :)
	} // switch(state)...
	return flags;
}

