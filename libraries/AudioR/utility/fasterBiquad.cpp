
#include "Arduino.h"
#include "dspinst.h"
#include "fasterBiquad.h"

float buff[128];
#define BQ_B0 0
#define BQ_B1 1
#define BQ_B2 2
#define BQ_A1 3
#define BQ_A2 4
#define BQ_BPREV1 5
#define BQ_BPREV2 6
#define BQ_APREV1 7
#define BQ_APREV2 8
#define BQ_SUM 9

float peakBiquads(int16_t* buf16, float* coefs)
{
	float sample,b0,b1,b2,a1,a2,aprev1,aprev2,bprev1,bprev2,sum;
	float *prevs_reset,*buffreset,*buffp=&buff[0],*buffend=&buff[0]+128;
	int16_t samint,max=-32868,min=32767;
	int16_t *buf16reset=buf16,*buf16end=buf16+128;

	if(coefs[BQ_SUM]==FILTER_COEFS_PROMISE)
	{
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		buffreset=buffp;
		do { // int => float
			samint=*buf16++;
			if(samint<min) min=samint;
			if(samint>max) max=samint;
			sample=(float)*samint;

			
			
			
//			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			asm volatile("vmul.f32 %1, %2, %3" : "=s" (sum) : "s" (sample), "s" (b0));

			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;

			aprev2=aprev1;

			aprev1=sum+b2*bprev2;

			bprev2=bprev1;
			bprev1=sample;
			*buffp++=aprev1;
		} while(buffp<buffend);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
		while(coefs[10]==FILTER_COEFS_PROMISE) { // we look ahead to the next filter after this one, if there are only two we need to skip float->float
			coefs++;
			b0=*coefs++;
			b1=*coefs++;
			b2=*coefs++;
			a1=*coefs++;
			a2=*coefs++;
			prevs_reset=coefs;
			aprev1=*coefs++;
			aprev2=*coefs++;
			bprev1=*coefs++;
			bprev2=*coefs++;
			buffp=buffreset;
			do { // float => float
				sample=*buffp;
				sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
				sum+=b1*bprev1;
				sum+=a1*aprev1;
				sum+=a2*aprev2;
				aprev2=aprev1;
				aprev1=sum+b2*bprev2;
				bprev2=bprev1;
				bprev1=sample;
				*buffp++=aprev1;
			} while(buffp<buffend);
			*prevs_reset++=aprev1;
			*prevs_reset++=aprev2;
			*prevs_reset++=bprev1;
			*prevs_reset=bprev2;
		}
		coefs++;
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		buffp=buffreset;
		buf16=buf16reset;
		do { // float => int
			sample=*buffp++;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buf16++=saturate16((int32_t)aprev1);
		} while(buf16<buf16end);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
	} else { // only one to do...
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		do { // int -> int
			samint=*buf16;
			if(samint<min) min=samint;
			if(samint>max) max=samint;
			sample=(float)*samint;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buf16++=saturate16((int32_t)aprev1);
		} while(buf16<buf16end);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
	}
	return (max-min)/65535.0f;
}

void suckBiquads(int16_t* buf16, float* coefs)
{
	float sample,b0,b1,b2,a1,a2,aprev1,aprev2,bprev1,bprev2,sum;
	float *prevs_reset,*buffreset,*buffp=&buff[0],*buffend=&buff[0]+128;
	int16_t *buf16reset=buf16,*buf16end=buf16+128;
	
	if(coefs[BQ_SUM]==FILTER_COEFS_PROMISE)
	{
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		buffreset=buffp;
		do { // int => float
			sample=(float)*buf16++;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buffp++=aprev1;
		} while(buffp<buffend);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
		while(coefs[10]==FILTER_COEFS_PROMISE) { // we look ahead to the next filter after this one, if there are only two we need to skip float->float
			coefs++;
			b0=*coefs++;
			b1=*coefs++;
			b2=*coefs++;
			a1=*coefs++;
			a2=*coefs++;
			prevs_reset=coefs;
			aprev1=*coefs++;
			aprev2=*coefs++;
			bprev1=*coefs++;
			bprev2=*coefs++;
			buffp=buffreset;
			do { // float => float
				sample=*buffp;
				sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
				sum+=b1*bprev1;
				sum+=a1*aprev1;
				sum+=a2*aprev2;
				aprev2=aprev1;
				aprev1=sum+b2*bprev2;
				bprev2=bprev1;
				bprev1=sample;
				*buffp++=aprev1;
			} while(buffp<buffend);
			*prevs_reset++=aprev1;
			*prevs_reset++=aprev2;
			*prevs_reset++=bprev1;
			*prevs_reset=bprev2;
		}
		coefs++;
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		buffp=buffreset;
		buf16=buf16reset;
		do { // float => int
			sample=*buffp++;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buf16++=saturate16((int32_t)aprev1);
		} while(buf16<buf16end);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
	} else { // only one to do...
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		do { // int -> int
			sample=(float)*buf16;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buf16++=saturate16((int32_t)aprev1);
		} while(buf16<buf16end);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
	}
}

/*


FASTRUN floatbiquadfloat(float* buffer, float* coefs)
{
	register float sample,b0,b1,b2,a1,a2,sum,aprev1,aprev2,bprev1,bprev2;
	float *buffer_reset=buffer, *prevs_reset,*buffer_end=buffer+128;
	do {
		b0=*coefs++;
		b1=*coefs++;
		b2=*coefs++;
		a1=*coefs++;
		a2=*coefs++;
		prevs_reset=coefs;
		aprev1=*coefs++;
		aprev2=*coefs++;
		bprev1=*coefs++;
		bprev2=*coefs++;
		do {
			sample=*buffer;
			sum=b0*sample; // temp=aprev1; aprev1=(b0*sample)+(b1*bprev1)+(b2*bprev2)+(a1*aprev1)+(a2*aprev2); aprev2=temp;
			sum+=b1*bprev1;
			sum+=a1*aprev1;
			sum+=a2*aprev2;
			aprev2=aprev1;
			aprev1=sum+b2*bprev2;
			bprev2=bprev1;
			bprev1=sample;
			*buffer++=aprev1;
		} while(buffer<buffer_end);
		*prevs_reset++=aprev1;
		*prevs_reset++=aprev2;
		*prevs_reset++=bprev1;
		*prevs_reset=bprev2;
		buffer=buffer_reset;
	} while(*coefs++==373592); // if the 'sum' of this group of coefs is the floating point equivalent of deadbeaf there has to be another filter to follow
} // 

FASTRUN float* intbiquadfloat(float* buffer, float* coefs, int16_t* source)
{
	register float sample,b0,b1,b2,a1,a2,sum,aprev1,aprev2,bprev1,bprev2;
	float *buffer_reset=buffer, *prevs_reset,*buffer_end=buffer+128;
	
	b0=*coefs++;
	b1=*coefs++;
	b2=*coefs++;
	a1=*coefs++;
	a2=*coefs++;
	prevs_reset=coefs;
	aprev1=*coefs++;
	aprev2=*coefs++;
	bprev1=*coefs++;
	bprev2=*coefs++;
	do {
		sample=inversion*(*source++);
		sum=b0*sample;
		sum+=b1*bprev1;
		sum+=a1*aprev1;
		sum+=a2*aprev2;
		aprev2=aprev1;
		aprev1=sum+b2*bprev2;
		bprev2=bprev1;
		bprev1=sample;
		*buffer++=aprev1;
	} while(buffer<buffer_end);
	*prevs_reset++=aprev1;
	*prevs_reset++=aprev2;
	*prevs_reset++=bprev1;
	*prevs_reset=bprev2;
	
	return (++coefs); // they will need this passed back to them if they are using me.
}
*/

void calcBiquad(uint8_t type, float Fc, float Q, float peakGain, float * coefs, float Fs)
{
	float n[5]={1,0,0,0,0};

	double norm;

	double V = pow(10, fabs(peakGain) / 20);
	double K = tan(M_PI * Fc / Fs);
	switch (type) {
/*    case "one-pole lp":
      coefs[3] = Math.exp(-2.0 * Math.PI * (Fc / Fs));
            coefs[0] = 1.0 - coefs[3];
            coefs[3] = -coefs[3];
      coefs[1] = coefs[2] = coefs[4] = 0;
      break;
            
    case "one-pole hp":
      coefs[3] = -Math.exp(-2.0 * Math.PI * (0.5 - Fc / Fs));
            coefs[0] = 1.0 + coefs[3];
            coefs[3] = -coefs[3];
      coefs[1] = coefs[2] = coefs[4] = 0;
      break;
                                  */
    case FLT_LOPASS:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = K * K * norm;
      n[1] = 2 * n[0];
      n[2] = n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case FLT_HIPASS:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = 1 * norm;
      n[1] = -2 * n[0];
      n[2] = n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case FLT_BANDPASS:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = K / Q * norm;
      n[1] = 0;
      n[2] = -n[0];
      n[3] = 2 * (K * K - 1) * norm;
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case FLT_NOTCH:
      norm = 1 / (1 + K / Q + K * K);
      n[0] = (1 + K * K) * norm;
      n[1] = 2 * (K * K - 1) * norm;
      n[2] = n[0];
      n[3] = n[1];
      n[4] = (1 - K / Q + K * K) * norm;
    break;
    
    case FLT_PARAEQ:
      if (peakGain >= 0) {
        norm = 1 / (1 + 1/Q * K + K * K);
        n[0] = (1 + V/Q * K + K * K) * norm;
        n[1] = 2 * (K * K - 1) * norm;
        n[2] = (1 - V/Q * K + K * K) * norm;
        n[3] = n[1];
        n[4] = (1 - 1/Q * K + K * K) * norm;
    } else {  
        norm = 1 / (1 + V/Q * K + K * K);
        n[0] = (1 + 1/Q * K + K * K) * norm;
        n[1] = 2 * (K * K - 1) * norm;
        n[2] = (1 - 1/Q * K + K * K) * norm;
        n[3] = n[1];
        n[4] = (1 - V/Q * K + K * K) * norm;
    }
    break;
    case FLT_LOSHELF:
		if (peakGain >= 0) {
			norm = 1 / (1 + M_SQRT2 * K + K * K);
			n[0] = (1 + sqrt(2*V) * K + V * K * K) * norm;
			n[1] = 2 * (V * K * K - 1) * norm;
			n[2] = (1 - sqrt(2*V) * K + V * K * K) * norm;
			n[3] = 2 * (K * K - 1) * norm;
			n[4] = (1 - M_SQRT2 * K + K * K) * norm;
		} else {  
			norm = 1 / (1 + sqrt(2*V) * K + V * K * K);
			n[0] = (1 + M_SQRT2 * K + K * K) * norm;
			n[1] = 2 * (K * K - 1) * norm;
			n[2] = (1 - M_SQRT2 * K + K * K) * norm;
			n[3] = 2 * (V * K * K - 1) * norm;
			n[4] = (1 - sqrt(2*V) * K + V * K * K) * norm;
		}
    break;
    case FLT_HISHELF:
        if (peakGain >= 0) {
			norm = 1 / (1 + M_SQRT2 * K + K * K);
			n[0] = (V + sqrt(2*V) * K + K * K) * norm;
			n[1] = 2 * (K * K - V) * norm;
			n[2] = (V - sqrt(2*V) * K + K * K) * norm;
			n[3] = 2 * (K * K - 1) * norm;
			n[4] = (1 - M_SQRT2 * K + K * K) * norm;
        } else {  
			norm = 1 / (V + sqrt(2*V) * K + K * K);
			n[0] = (1 + M_SQRT2 * K + K * K) * norm;
			n[1] = 2 * (K * K - 1) * norm;
			n[2] = (1 - M_SQRT2 * K + K * K) * norm;
			n[3] = 2 * (K * K - V) * norm;
			n[4] = (V - sqrt(2*V) * K + K * K) * norm;
		}
	}

	(NVIC_DISABLE_IRQ(IRQ_SOFTWARE)); // __disable_irq(); // nasty could happen if we get half way through changing these
	coefs[0]=n[0];
	coefs[1]=n[1];
	coefs[2]=n[2]; // (because the way I use them, they could (*should* actually) be writing a live filter)
	coefs[3]=-n[3];
	coefs[4]=-n[4];
	(NVIC_ENABLE_IRQ(IRQ_SOFTWARE)); // __enable_irq(); // and the audio interrupt fires.
	
	
}
